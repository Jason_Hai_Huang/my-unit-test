package hai.exercise.myunittest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    @Mock
    private LoginService services;
    @Mock
    private LoginView view;

    private LoginPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new LoginPresenter(view, services);
    }


    @Test
    public void shouldShowErrorMsgWhenUsernameIsEmpty() {
        when(view.getUsername()).thenReturn("");
        presenter.onLoginClicked();

        verify(view).showUsernameError(R.string.username_error);
    }

    @Test
    public void shouldShowErrorMsgWhenPasswordIsEmpty() {
        when(view.getUsername()).thenReturn("Hai");
        when(view.getPassword()).thenReturn("");
        presenter.onLoginClicked();

        verify(view).showPasswordError(R.string.password_error);
    }

    @Test
    public void shouldShowLoginSuccessfulMsgWhenUsernameAndPasswordAreCorrect() {
        when(view.getUsername()).thenReturn("Hai");
        when(view.getPassword()).thenReturn("123");
        when(services.login("Hai", "123")).thenReturn(true);
        presenter.onLoginClicked();

        verify(view).showLoginSuccessfulMsg();
    }

    @Test
    public void shouldShowLoginFailMsgWhenUsernameOrPasswordAreWrong() {
        when(view.getUsername()).thenReturn("Hai");
        when(view.getPassword()).thenReturn("111");
        when(services.login("Hai", "111")).thenReturn(false);
        presenter.onLoginClicked();

        verify(view).showLoginFailMsg();
    }

    @After
    public void tearDown() throws Exception {
    }

}