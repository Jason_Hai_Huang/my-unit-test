package hai.exercise.myunittest;

class LoginPresenter {

    private LoginView mView;
    private LoginService mService;

    public LoginPresenter(LoginView view, LoginService service){

        mView = view;
        mService = service;
    }

    public void onLoginClicked() {
        String username = mView.getUsername();
        if (username.isEmpty()){
            mView.showUsernameError(R.string.username_error );
            return;
        }
        String password = mView.getPassword();
        if (password.isEmpty()){
            mView.showPasswordError(R.string.password_error);
            return;
        }

        boolean loggedIn = mService.login(username, password);
        if (loggedIn){
            mView.showLoginSuccessfulMsg();
        }else{
            mView.showLoginFailMsg();
        }

    }
}
