package hai.exercise.myunittest;

interface LoginView {

    String getUsername();
    void showUsernameError(int resId);

    String getPassword();
    void showPasswordError(int resId);

    void showLoginSuccessfulMsg();

    void showLoginFailMsg();
}
