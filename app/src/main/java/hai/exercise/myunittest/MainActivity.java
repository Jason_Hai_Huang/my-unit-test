package hai.exercise.myunittest;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LoginView {

    private Context mContext;
    private LoginPresenter presenter;
    private EditText etUsername;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mContext = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);

        presenter = new LoginPresenter(this, new LoginService());

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onLoginClicked();
              }
        });

    }

    @Override
    public String getUsername() {
        return etUsername.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {
        etUsername.setError(getString(resId));
    }

    @Override
    public String getPassword() {
        return etPassword.getText().toString();
    }

    @Override
    public void showPasswordError(int resId) {
        etPassword.setError(getString(resId));
    }

    @Override
    public void showLoginSuccessfulMsg() {
        Toast.makeText(mContext, R.string.login_successful, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoginFailMsg() {
        Toast.makeText(mContext, R.string.login_failed, Toast.LENGTH_LONG).show();
    }
}
